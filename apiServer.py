
from flask_api import FlaskAPI, status, exceptions
import json
import os
from utils.songsUtils import SongsUtils

app = FlaskAPI(__name__)
utils = SongsUtils()


try:
    port = int(os.environ['PORT'])
except:
    port = 5000


# get all songs names
@app.route("/all/songs/", methods=['GET'])
def songs_list():
    """
    List of songs.
    """
    songs = utils.getSongsList()
    return json.dumps(songs)


@app.route("/song/by/artist/<string:artist>/", methods=['GET'])
def getSongByArtistName(artist):
    """
    Get song by artist name
    """
    songs = utils.getSongsByArtist(artist)
    return json.dumps(songs)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port)